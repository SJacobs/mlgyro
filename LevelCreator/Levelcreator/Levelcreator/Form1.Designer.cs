﻿namespace Levelcreator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnup = new System.Windows.Forms.Button();
            this.btndown = new System.Windows.Forms.Button();
            this.btnright = new System.Windows.Forms.Button();
            this.btnleft = new System.Windows.Forms.Button();
            this.radioUp = new System.Windows.Forms.RadioButton();
            this.radioDown = new System.Windows.Forms.RadioButton();
            this.radioLeft = new System.Windows.Forms.RadioButton();
            this.radioRight = new System.Windows.Forms.RadioButton();
            this.gpdirection = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.radioNone = new System.Windows.Forms.RadioButton();
            this.listBox1 = new System.Windows.Forms.ListBox();
            this.gpdirection.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnup
            // 
            this.btnup.Location = new System.Drawing.Point(242, 34);
            this.btnup.Name = "btnup";
            this.btnup.Size = new System.Drawing.Size(193, 121);
            this.btnup.TabIndex = 0;
            this.btnup.Text = "up";
            this.btnup.UseVisualStyleBackColor = true;
            this.btnup.Click += new System.EventHandler(this.btnup_Click);
            // 
            // btndown
            // 
            this.btndown.Location = new System.Drawing.Point(242, 316);
            this.btndown.Name = "btndown";
            this.btndown.Size = new System.Drawing.Size(193, 121);
            this.btndown.TabIndex = 1;
            this.btndown.Text = "downy";
            this.btndown.UseVisualStyleBackColor = true;
            this.btndown.Click += new System.EventHandler(this.btndown_Click);
            // 
            // btnright
            // 
            this.btnright.Location = new System.Drawing.Point(452, 174);
            this.btnright.Name = "btnright";
            this.btnright.Size = new System.Drawing.Size(193, 121);
            this.btnright.TabIndex = 2;
            this.btnright.Text = "btnright";
            this.btnright.UseVisualStyleBackColor = true;
            this.btnright.Click += new System.EventHandler(this.btnright_Click);
            // 
            // btnleft
            // 
            this.btnleft.Location = new System.Drawing.Point(33, 174);
            this.btnleft.Name = "btnleft";
            this.btnleft.Size = new System.Drawing.Size(193, 121);
            this.btnleft.TabIndex = 3;
            this.btnleft.Text = "lefto";
            this.btnleft.UseVisualStyleBackColor = true;
            this.btnleft.Click += new System.EventHandler(this.btnleft_Click);
            // 
            // radioUp
            // 
            this.radioUp.AutoSize = true;
            this.radioUp.Location = new System.Drawing.Point(48, 19);
            this.radioUp.Name = "radioUp";
            this.radioUp.Size = new System.Drawing.Size(62, 17);
            this.radioUp.TabIndex = 4;
            this.radioUp.TabStop = true;
            this.radioUp.Text = "radioUp";
            this.radioUp.UseVisualStyleBackColor = true;
            // 
            // radioDown
            // 
            this.radioDown.AutoSize = true;
            this.radioDown.Location = new System.Drawing.Point(48, 42);
            this.radioDown.Name = "radioDown";
            this.radioDown.Size = new System.Drawing.Size(76, 17);
            this.radioDown.TabIndex = 5;
            this.radioDown.TabStop = true;
            this.radioDown.Text = "radioDown";
            this.radioDown.UseVisualStyleBackColor = true;
            // 
            // radioLeft
            // 
            this.radioLeft.AutoSize = true;
            this.radioLeft.Location = new System.Drawing.Point(48, 65);
            this.radioLeft.Name = "radioLeft";
            this.radioLeft.Size = new System.Drawing.Size(66, 17);
            this.radioLeft.TabIndex = 6;
            this.radioLeft.TabStop = true;
            this.radioLeft.Text = "radioLeft";
            this.radioLeft.UseVisualStyleBackColor = true;
            // 
            // radioRight
            // 
            this.radioRight.AutoSize = true;
            this.radioRight.Location = new System.Drawing.Point(48, 88);
            this.radioRight.Name = "radioRight";
            this.radioRight.Size = new System.Drawing.Size(73, 17);
            this.radioRight.TabIndex = 7;
            this.radioRight.TabStop = true;
            this.radioRight.Text = "radioRight";
            this.radioRight.UseVisualStyleBackColor = true;
            // 
            // gpdirection
            // 
            this.gpdirection.Controls.Add(this.radioNone);
            this.gpdirection.Controls.Add(this.radioUp);
            this.gpdirection.Controls.Add(this.radioRight);
            this.gpdirection.Controls.Add(this.radioDown);
            this.gpdirection.Controls.Add(this.radioLeft);
            this.gpdirection.Location = new System.Drawing.Point(26, 22);
            this.gpdirection.Name = "gpdirection";
            this.gpdirection.Size = new System.Drawing.Size(200, 133);
            this.gpdirection.TabIndex = 8;
            this.gpdirection.TabStop = false;
            this.gpdirection.Text = "direction";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(561, 496);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 9;
            this.button1.Text = "PRINT";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(480, 496);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 10;
            this.button2.Text = "CLEAR";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // radioNone
            // 
            this.radioNone.AutoSize = true;
            this.radioNone.Location = new System.Drawing.Point(48, 110);
            this.radioNone.Name = "radioNone";
            this.radioNone.Size = new System.Drawing.Size(74, 17);
            this.radioNone.TabIndex = 8;
            this.radioNone.TabStop = true;
            this.radioNone.Text = "radioNone";
            this.radioNone.UseVisualStyleBackColor = true;
            this.radioNone.CheckedChanged += new System.EventHandler(this.radioNone_CheckedChanged);
            // 
            // listBox1
            // 
            this.listBox1.FormattingEnabled = true;
            this.listBox1.Location = new System.Drawing.Point(19, 466);
            this.listBox1.Name = "listBox1";
            this.listBox1.Size = new System.Drawing.Size(435, 134);
            this.listBox1.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(693, 617);
            this.Controls.Add(this.listBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.gpdirection);
            this.Controls.Add(this.btnleft);
            this.Controls.Add(this.btnright);
            this.Controls.Add(this.btndown);
            this.Controls.Add(this.btnup);
            this.Name = "Form1";
            this.Text = "Form1";
            this.gpdirection.ResumeLayout(false);
            this.gpdirection.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnup;
        private System.Windows.Forms.Button btndown;
        private System.Windows.Forms.Button btnright;
        private System.Windows.Forms.Button btnleft;
        private System.Windows.Forms.RadioButton radioUp;
        private System.Windows.Forms.RadioButton radioDown;
        private System.Windows.Forms.RadioButton radioLeft;
        private System.Windows.Forms.RadioButton radioRight;
        private System.Windows.Forms.GroupBox gpdirection;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RadioButton radioNone;
        private System.Windows.Forms.ListBox listBox1;
    }
}

