﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Levelcreator
{
    public partial class Form1 : Form
    {
        private List<string> strings = new List<string>();

        public Form1()
        {
            InitializeComponent();
            strings.Add("addTile(Kind.START, Direction.NONE, x, y);");
        }

        private void btnup_Click(object sender, EventArgs e)
        {
          
            strings.Add("y -= 200;");
            strings.Add(checkDirection());
            fillList();
        }

        private void fillList()
        {
            listBox1.Items.Clear();
            foreach (string text in strings)
            {
                listBox1.Items.Add(text);
            }
        }

        private void btnright_Click(object sender, EventArgs e)
        {
            strings.Add("x += 200;");
            strings.Add(checkDirection());
            fillList();
        }

        private void btndown_Click(object sender, EventArgs e)
        {
            strings.Add("y += 200;");
            strings.Add(checkDirection());
            fillList();
        }

        private void btnleft_Click(object sender, EventArgs e)
        {
            strings.Add("x -= 200;");
            strings.Add(checkDirection());
            fillList();
        }

        public string checkDirection()
        {
            if (radioUp.Checked)
            {
                return "addTile(Kind.NORMAL, Direction.UP, x, y);";
            }
            if (radioDown.Checked)
            {
                return "addTile(Kind.NORMAL, Direction.DOWN, x, y);";
            }
            if (radioLeft.Checked)
            {
                return "addTile(Kind.NORMAL, Direction.LEFT, x, y);";
            }
            if (radioRight.Checked)
            {
                return "addTile(Kind.NORMAL, Direction.RIGHT, x, y);";
            }
            if (radioNone.Checked)
            {
                return "addTile(Kind.NORMAL, Direction.NONE, x, y);";
            }
            return "addTile(Kind.NORMAL, Direction.NONE, x, y);";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Console.WriteLine("===================================================================");
            foreach (string text in strings)
                Console.WriteLine(text);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            strings.Clear();
        }

        private void radioNone_CheckedChanged(object sender, EventArgs e)
        {

        }
    }
}
