﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MLGyro.MLGyroGame.Levels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MLGyro.MLGyro
{
    public class Airhockey : AbstractGame
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        //ball
        Texture2D ballSprite;
        Vector2 ballPosition = new Vector2(925, 500);
        Vector2 ballspeed = new Vector2(150, 150);
        int ballRadius;
        float ballCentre_X;
        float ballCentre_Y;
        int ballmass;

        //left paddle
        Texture2D paddleLeftSprite;
        Vector2 paddleLeftPosition = new Vector2(250, 500);
        Vector2 paddleLeftSpeed;
        int paddleLeftRadius;
        float paddleLeftCentre_X;
        float paddleLeftCentre_Y;
        int paddleLeftMass;

        //right paddle
        Texture2D paddleRightSprite;
        Vector2 paddleRightPosition = new Vector2(1550, 500);
        Vector2 paddleRightSpeed;
        int paddleRightRadius;
        float paddleRightCentre_X;
        float paddleRightCentre_Y;
        int paddleRightMass;

        //overige informatie
        SpriteFont font;
        int score = 0;
        int score2 = 0;

       // Texture2D goalRightSprite;
        Vector2 goalRightPosition = new Vector2(1835, 400);
       // Texture2D goalLeftSprite;
        Vector2 goalLeftPosition = new Vector2(0, 400);
       // Texture2D lineSprite;
        Vector2 linePosition = new Vector2(350, -65);

        float newVelBall_X;
        float newVelBall_Y;

        double distance;
        double distance2;

        private Main main;
        public Airhockey(Main main, Ball ball)
        {
            this.main = main;
           // this.ball = ball;
        }


        public override void Draw(SpriteBatch spriteBatch, SpriteFont font, ContentManager content)
        {
            spriteBatch.DrawString(font, "Score: " + score, new Vector2(100, 100), Color.Black);
            spriteBatch.DrawString(font, "Score: " + score2, new Vector2(1500, 100), Color.Black);
            spriteBatch.Draw(main.goalRightSprite, goalRightPosition, Color.White);
            spriteBatch.Draw(main.goalLeftSprite, goalLeftPosition, Color.White);

            spriteBatch.Draw(main.lineSprite, linePosition, Color.White);
            spriteBatch.Draw(main.ballSprite, ballPosition, Color.White);
            spriteBatch.Draw(main.paddleRightSprite, paddleRightPosition, Color.White);
            spriteBatch.Draw(main.paddleLeftSprite, paddleLeftPosition, Color.White);

        }

        public void CheckBallWall()
        {
            int maxX = main.GraphicsDevice.Viewport.Width - (main.ballSprite.Width + 10);
            int maxY = main.GraphicsDevice.Viewport.Height - (main.ballSprite.Height + 10);
            if (ballPosition.X > maxX || ballPosition.X < 0)
            {
                ballspeed.X *= -1;
            }
            if (ballPosition.Y > maxY || ballPosition.Y < 0)
            {
                ballspeed.Y *= -1;
                if ((ballPosition.Y - 20) > maxY)
                {
                    ballPosition.Y = 500;
                }
            }
        }

        public void CheckPaddleLeftWall()
        {
            int maxPaddleX = main.GraphicsDevice.Viewport.Width - main.paddleRightSprite.Width;
            int maxPaddleY = main.GraphicsDevice.Viewport.Height - main.paddleRightSprite.Height;

            if (paddleLeftPosition.X > (main.GraphicsDevice.Viewport.Width - main.paddleLeftSprite.Width) / 2)
            {
                paddleLeftPosition.X = (main.GraphicsDevice.Viewport.Width - main.paddleLeftSprite.Width) / 2;
            }
            if (paddleLeftPosition.X < 0)
            {
                paddleLeftPosition.X = 0;
            }
            if (paddleLeftPosition.Y > maxPaddleY)
            {
                paddleLeftPosition.Y = main.GraphicsDevice.Viewport.Height - main.paddleLeftSprite.Height;
            }
            if (paddleLeftPosition.Y < 0)
            {
                paddleLeftPosition.Y = 0;
            }
        }

        public void CheckPaddleRightWall()
        {
            int maxPaddleX = main.GraphicsDevice.Viewport.Width - main.paddleRightSprite.Width;
            int maxPaddleY = main.GraphicsDevice.Viewport.Height - main.paddleRightSprite.Height;

            if (paddleRightPosition.X < (main.GraphicsDevice.Viewport.Width - main.paddleRightSprite.Width) / 2)
            {
                paddleRightPosition.X = (main.GraphicsDevice.Viewport.Width - main.paddleRightSprite.Width) / 2;
            }
            if (paddleRightPosition.X > maxPaddleX)
            {
                paddleRightPosition.X = maxPaddleX;
            }
            if (paddleRightPosition.Y > maxPaddleY)
            {
                paddleRightPosition.Y = main.GraphicsDevice.Viewport.Height - main.paddleRightSprite.Height;
            }
            if (paddleRightPosition.Y < 0)
            {
                paddleRightPosition.Y = 0;
            }
        }

        private int CircleCollision()
        {
            ballRadius = main.ballSprite.Width / 2;
            ballmass = ballRadius;
            ballCentre_X = ballPosition.X + (main.ballSprite.Width / 2);
            ballCentre_Y = ballPosition.Y + (main.ballSprite.Height / 2);

            paddleLeftMass = paddleLeftRadius;
            paddleLeftRadius = main.paddleLeftSprite.Width / 2;
            paddleLeftCentre_X = paddleLeftPosition.X + (main.paddleLeftSprite.Width / 2);
            paddleLeftCentre_Y = paddleLeftPosition.Y + (main.paddleLeftSprite.Height / 2);

            paddleRightRadius = main.paddleRightSprite.Width / 2;
            paddleRightMass = paddleRightRadius;
            paddleRightCentre_X = paddleRightPosition.X + (main.paddleRightSprite.Width / 2);
            paddleRightCentre_Y = paddleRightPosition.Y + (main.paddleRightSprite.Height / 2);


            distance = Math.Sqrt(((ballCentre_X - paddleRightCentre_X) * (ballCentre_X - paddleRightCentre_X)
                + ((ballCentre_Y - paddleRightCentre_Y) * (ballCentre_Y - paddleRightCentre_Y))));

            distance2 = Math.Sqrt(((ballCentre_X - paddleLeftCentre_X) * (ballCentre_X - paddleLeftCentre_X) + ((ballCentre_Y - paddleLeftCentre_Y) * (ballCentre_Y - paddleLeftCentre_Y))));


            if (distance < (ballRadius + paddleRightRadius))
            {
                return 0;
            }
            if (distance2 < (ballRadius + paddleLeftRadius))
            {
                return 1;
            }
            else
            {
                return 2;
            }
        }

        public void Bounce()
        {
            newVelBall_X = (ballspeed.X * (ballmass - paddleRightMass) +
                (2 * paddleRightMass * paddleRightSpeed.X)) /
                (ballmass + paddleRightMass);
            ballspeed.X += (newVelBall_X + main.ballSprite.Width / 2);

            newVelBall_Y = (ballspeed.Y * (ballmass - paddleRightMass) +
                (2 * paddleRightMass * paddleRightSpeed.Y)) /
                (ballmass + paddleRightMass);
            ballspeed.Y += (newVelBall_Y + main.ballSprite.Height / 2);
        }

        public void Bounce2()
        {
            newVelBall_X = (ballspeed.X * (ballmass - paddleLeftMass) +
                (2 * paddleLeftMass * paddleLeftSpeed.X)) / (ballmass + paddleLeftMass);
            ballspeed.X += (newVelBall_X + main.ballSprite.Width / 2);

            newVelBall_Y = (ballspeed.Y * (ballmass - paddleLeftMass) +
                (2 * paddleLeftMass * paddleLeftSpeed.Y)) / (ballmass + paddleLeftMass);
            ballspeed.Y += (newVelBall_Y + main.ballSprite.Height / 2);
        }

        public static int x;
        public static int y;
        public static double xboxX;
        public static double xboxY;
        public override void Update(GameTime gameTime)
        {
            Console.WriteLine("GOT HERE");
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                main.Exit();

            // TODO: Add your update logic here
            ballPosition += ballspeed * (float)gameTime.ElapsedGameTime.TotalSeconds;
            paddleRightSpeed = new Vector2(200, 200);
            paddleLeftSpeed = new Vector2(200, 200);

            paddleLeftPosition.X += x;
            paddleLeftPosition.Y += y;

            paddleRightPosition.X += (float)(xboxX * 20);
            paddleRightPosition.Y -= (float)(xboxY * 20);

            KeyboardState state = Keyboard.GetState();
             if (state.IsKeyDown(Keys.Left))
             {
                 paddleRightPosition.X -= 20;
                 paddleRightSpeed.X *= -1;
             }
             if (state.IsKeyDown(Keys.Right))
             {
                 paddleRightPosition.X += 20;
             }
             if (state.IsKeyDown(Keys.Up))
             {
                 paddleRightPosition.Y -= 20;
                 paddleRightSpeed.Y *= -1;
             }
             if (state.IsKeyDown(Keys.Down))
             {
                 paddleRightPosition.Y += 20;
             }

             if (state.IsKeyDown(Keys.A))
             {
                 paddleLeftPosition.X -= 20;
                 paddleLeftSpeed.X *= -1;
             }
             if (state.IsKeyDown(Keys.D))
             {
                 paddleLeftPosition.X += 20;
             }
             if (state.IsKeyDown(Keys.W))
             {
                 paddleLeftPosition.Y -= 20;
                 paddleLeftSpeed.Y *= -1;
             }
             if (state.IsKeyDown(Keys.S))
             {
                 paddleLeftPosition.Y += 20;
             }

            CheckBallWall();
            CheckPaddleLeftWall();
            CheckPaddleRightWall();

            Rectangle ball = new Rectangle(Convert.ToInt32(ballPosition.X), Convert.ToInt32(ballPosition.Y), main.ballSprite.Width, main.ballSprite.Height);
            Rectangle goalRight = new Rectangle(Convert.ToInt32(goalRightPosition.X), Convert.ToInt32(goalRightPosition.Y), main.goalRightSprite.Width, main.goalRightSprite.Height);
            Rectangle goalLeft = new Rectangle(Convert.ToInt32(goalLeftPosition.X), Convert.ToInt32(goalLeftPosition.Y), main.goalLeftSprite.Width, main.goalLeftSprite.Height);

            if (ball.Intersects(goalRight))
            {
                ballPosition.X = 925; ballPosition.Y = 500;
                ballspeed.X = 0; ballspeed.Y = 0;
                score += 1;
            }
            if (ball.Intersects(goalLeft))
            {
                ballPosition.X = 925; ballPosition.Y = 500;
                ballspeed.X = 0; ballspeed.Y = 0;
                score2 += 1;
            }


            if (CircleCollision() == 0)
            {
                Bounce();
            }
            if (CircleCollision() == 1)
            {
                Bounce2();
            }

        }
    }
}
