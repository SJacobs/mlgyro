﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MLGyro
{
    public class Ball : Sprite
    {
        public Direction direction;

        public Ball(Texture2D image, Vector2 position, Direction direction)
            : base(image, position)
        {
            this.image = image;
            this.position = position;
            //this.direction = direction;
        }
    }
}
