﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MLGyro
{
    public class Button : Sprite
    {
        public Direction direction;
        public string text;

        public Button(Texture2D image, Vector2 position, string text)
            : base(image, position)
        {
            this.image = image;
            this.position = position;
            this.text = text;
        }

        /*public void DrawButton(SpriteBatch spriteBatch, SpriteFont font)
        {
            spriteBatch.Draw(image, position, Color.White);
            spriteBatch.DrawString(font, text, position, Color.Black);
        }*/
    }
}