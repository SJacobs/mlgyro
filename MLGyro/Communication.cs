﻿using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Linq;
using System.Text;

namespace MLGyro
{
    public class Communication
    {
        private const int connectionSpeed = 9600;
        private const String messageBeginMarker = "$";
        //private const String messageEndMarker = "%";
        public SerialPort serialPort;
        private string message;
        private const string COM_PORT = "COM8";

        public static Dictionary<Player, Controller> players = new Dictionary<Player, Controller>();


        public int gyroX;
        public int gyroY;
        public bool buttonPressed = false;
        /// <summary>
        /// This method gets called every 1 ms.
        /// </summary>
        /// 

        public string message1 = "";
        public const string START_SYMBOL = "$";
        public const string END_SYMBOL = "\n";
        public void tick()
        {
            if (!serialPort.IsOpen)
            {
                return;
            }
            if (serialPort.BytesToRead > 0)
            {
                string read = serialPort.ReadLine(); // serialPort.ReadExisting();
                HandleReceivedMessage(read);
            }
        }


        public void assignControllerToPlayer(Player player, Controller controller)
        {
            players.Add(player, controller);
        }

        public Controller getControllerByPlayer(Player player)
        {
            Controller controller;
            players.TryGetValue(player, out controller);
            return controller;
        }

        public void getControllerId()
        {
            SendMessage("#0.getPlayer");
        }

        public void getPlayerId()
        {
            SendMessage("#0.getPlayer1");
        }

        public int controllerId;
        public int sensorValue; // do something with the value in gyroball.

        public void HandleReceivedMessage(string message)
        {
            if (message.StartsWith("$2.buttonpressed"))
            {
                buttonPressed = true;
                return;
            }
            try {
                char[] spacesplit = { ' ' };
                string[] firstParse = message.Split(spacesplit, StringSplitOptions.RemoveEmptyEntries);
                message = firstParse[1];

                // append(read);
                char[] splits = { ',' };
                string[] texts = message.Split(splits, StringSplitOptions.RemoveEmptyEntries);

                Console.WriteLine(texts[1] + " " + texts[2]);

                gyroX = Convert.ToInt32(texts[1]);
                gyroY = Convert.ToInt32(texts[2]);
            }
            catch(Exception ex)
            {
                message1 = "";
            }
            
        }


        public void prepareArduino()
        {
            SendMessage("#2.setplayer 1");
            System.Threading.Thread.Sleep(1000);
            SendMessage("#2.sensactive 1");
        }

        public void HandleMessage(string message)
        {
            if (message.StartsWith("$") && message.EndsWith("\n"))
            {
                if (message.Contains("sens"))
                {
                    // parse sensor value and assign sensorValue to it.
                    Console.WriteLine(message);
                    return;
                }

                //TODO: Make parse method for this
                int beginIndex = message.IndexOf("$") + 1;
                int endIndex = message.IndexOf(".");
                int length = endIndex - beginIndex;

                string id = message.Substring(beginIndex, length);
                Console.WriteLine("ID:" + id);

                SendMessage("#" + id + ".setplayer " + 1/*speler id bepaald door game*/);
                SendMessage("#" + id + ".sensactive 1");
                return;
            }


            switch (message)
            {
                case "$ID.ControllerID":
                    // controllerId = ID;
                    break;
                case "$ID.player":
                    // player = ID;
                    break;
                case "$ID.sens":
                    //sensorValue = ...
                    break;
            }
        }

        public void parseReceivedMessages(string message)
        {
            Console.WriteLine(message);
            // string read = serialPort.ReadLine(); // serialPort.ReadExisting();
            if (message.StartsWith("$2.buttonpressed"))
            {
                buttonPressed = true;
            }
            if (message.StartsWith("$2.sens"))
            {
                char[] spacesplit = { ' ' };
                string[] firstParse = message.Split(spacesplit, StringSplitOptions.RemoveEmptyEntries);
                message = firstParse[1];

                // append(read);
                char[] splits = { ',' };
                string[] texts = message.Split(splits, StringSplitOptions.RemoveEmptyEntries);

                Console.WriteLine(texts[1] + " " + texts[2]);

                gyroX = Convert.ToInt32(texts[1]);
                gyroY = Convert.ToInt32(texts[2]);
            }

        }

        public Communication()
        {
            serialPort = new SerialPort();
            serialPort.BaudRate = connectionSpeed;
            message = "";
            openPort();
           // prepareArduino();
        }

        /// <summary>
        /// Open port.
        /// </summary>
        public void openPort()
        {
            serialPort.PortName = COM_PORT;
            serialPort.Open();
            if (serialPort.IsOpen)
            {
                serialPort.DiscardInBuffer();
                serialPort.DiscardOutBuffer();
            }
        }

        /// <summary>
        /// Clear message.
        /// </summary>
        public void clearMessage()
        {
            message = "";
        }

        /// <summary>
        /// Append bytes to message.
        /// </summary>
        /// <param name="text"></param>
        public void append(string text)
        {
            message += text;
        }

        /// <summary>
        /// Send a message to serial.
        /// </summary>
        /// <param name="message"></param>
        public void SendMessage(string message)
        {
            if (serialPort.IsOpen)
            {
                serialPort.WriteLine(message);
            }
        }
    }
}
