﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MLGyro
{
    public class DatabaseManager
    {
        private const string databaseFilename = "gyro-database.db";
        private SQLiteConnection connection = new SQLiteConnection("Data Source=" + databaseFilename + ";Version=3");

        public DatabaseManager()
        {
            //CreateDatebaseFile();
            // CreateTable();
        }

        public void OpenConnection()
        {
            if (connection.State != System.Data.ConnectionState.Open)
            {
                connection.Open();
            }
        }

        public void CloseConnection()
        {
            if (connection.State != System.Data.ConnectionState.Closed)
            {
                connection.Close();
            }
        }

        public void DeleteFromDatabase(int playerid)
        {
            ExecuteQuery("DELETE FROM scores WHERE playerid=" + playerid + ";");
        }

        public void ClearDatabase()
        {
            ExecuteQuery("DELETE * FROM scores;");
        }

        /// <summary>
        /// Just executes query, doesn't return anything.
        /// </summary>
        /// <param name="query"></param>
        public void ExecuteQuery(string query)
        {
            //createDatebaseFile();
            OpenConnection();
            SQLiteCommand command = new SQLiteCommand(query, connection);
            command.ExecuteNonQuery();
            CloseConnection();
        }

        /// <summary>
        /// Returns a list with the results from the query.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        /* public List<string> ExecuteReadQuery(string query)
         {
             OpenConnection();
             SQLiteCommand command = new SQLiteCommand(query, connection);
             SQLiteDataReader reader = command.ExecuteReader();

             List<string> results = new List<string>();

             // Iterates through all the results of reader.
             while (reader.Read())
             {
                 for (int i = 0; i < reader.FieldCount; i++)
                 {
                     string result = Convert.ToString(reader[i]);
                     results.Add(result);
                 }
             }

             CloseConnection();
             return results;
         }*/

        /// <summary>
        /// Returns a list with the results from the query.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public List<string> ExecuteReadQuery(string query)
        {
            OpenConnection();
            SQLiteCommand command = new SQLiteCommand(query, connection);
            SQLiteDataReader reader = command.ExecuteReader();

            List<string> results = new List<string>();

            // Iterates through all the results of reader.
            while (reader.Read())
            {
                string result = "";
                int words = reader.FieldCount;
                bool addSeperator = words > 1;
                for (int i = 0; i < words; i++)
                {
                    result += Convert.ToString(reader[i]);
                    if (addSeperator)
                    {
                        result += "$";
                    }
                }
                results.Add(result);
            }

            CloseConnection();
            return results;
        }

        /// <summary>
        /// Executes query and returns a single object.
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public object ExecuteScalarQuery(string query)
        {
            OpenConnection();
            SQLiteCommand command = new SQLiteCommand(query, connection);
            Object result = command.ExecuteScalar();
            CloseConnection();
            return result;
        }

        public void InsertHighscore(string playerName, string gameName, int level, int score)
        {
            int id = getIdForPlayer(playerName);
            int gameId = getIdForGame(gameName);
            ExecuteQuery("INSERT INTO Highscores(Player_ID, Game_ID, Level, Score) VALUES(" + id + ", " + gameId + ", " + level + ", " + score + ");");
        }

        private void InsertPlayer(string playerName, string password)
        {
            ExecuteQuery("INSERT INTO Players(Player_Name, Password) VALUES('" + playerName + "', '" + password + "');");
        }

        public bool tryLogin(string playerName, string password)
        {
            List<string> results = ExecuteReadQuery("SELECT * FROM Players WHERE Player_Name = '" + playerName + "';");
            if (results.Count <= 0)
            {
                InsertPlayer(playerName, password);
                return true;
            }
            List<string> passwordResults = ExecuteReadQuery("SELECT Password FROM Players WHERE Player_Name = '" + playerName + "' AND Password = '" + password + "';");
            if (passwordResults.Count <= 0)
            {
                return false;
            }
            if (passwordResults[0] == password)
            {
                return true;
            }
            return false;
        }

        public void UpdateLevelForGame(string playerName, int gameId, int level)
        {
            List<string> results = ExecuteReadQuery("SELECT Player_ID FROM Players WHERE Player_Name = '" + playerName + "';");
            foreach (string text in results)
            {
                Console.WriteLine(text);
            }
            int playerId = Convert.ToInt32(results[0]);
            Console.WriteLine(playerId);

            List<string> countResults = ExecuteReadQuery("SELECT COUNT(*) FROM Levels WHERE Player_ID = '" + playerId + "' AND Game_ID = " + gameId + ";");
            int count = Convert.ToInt32(countResults[0]);
            Console.WriteLine(count);

            if (count <= 0)
            {
                ExecuteQuery("INSERT INTO Levels(Player_ID, Game_ID, Level) VALUES(" + playerId + ", " + gameId + ", " + level + ")");
            }
            else
            {
                ExecuteQuery("UPDATE Levels SET Level = " + level + " WHERE Player_ID = '" + playerId + "' AND Game_ID = " + gameId + ";");
            }
        }

        public int getIdForGame(string gameName)
        {
            List<string> results = ExecuteReadQuery("SELECT Game_ID FROM Games WHERE Game_Name = '" + gameName + "';");
            if (results.Count >= 1)
            {
                Console.WriteLine(results[0]);
                return Convert.ToInt32(results[0]);
            }
            return -1;
        }

        public int getIdForPlayer(string playerName)
        {
            List<string> results = ExecuteReadQuery("SELECT Player_ID FROM Players WHERE Player_Name = '" + playerName + "';");
            if (results.Count >= 1)
            {
                return Convert.ToInt32(results[0]);
            }
            return -1;
        }

        /// <summary>
        /// Prints all results of table scores.
        /// </summary>
        public void PrintDatabase()
        {
            Console.WriteLine("---------------------------- results ----------------------------");
            List<string> results = ExecuteReadQuery("SELECT * FROM scores;");
            foreach (string result in results)
            {
                Console.WriteLine(result);
            }
        }

        /*public void CreateTable()
        {
            ExecuteQuery("CREATE TABLE IF NOT EXISTS scores(playerid INT, naam VARCHAR(100), score INT)");
        }

        public void CreateDatebaseFile()
        {
            if (!File.Exists(databaseFilename))
            {
                SQLiteConnection.CreateFile(databaseFilename);
            }
        }*/
    }
}
