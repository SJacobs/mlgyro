﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MLGyro
{
    public enum Direction
    {
        NONE, UP, DOWN, LEFT, RIGHT
    }
}
