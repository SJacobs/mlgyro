﻿namespace MLGyro
{
    partial class Highscores
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.listHighscores = new System.Windows.Forms.ListBox();
            this.lbHighscoresTitle = new System.Windows.Forms.Label();
            this.comboGame = new System.Windows.Forms.ComboBox();
            this.nudLevel = new System.Windows.Forms.NumericUpDown();
            this.lbGame = new System.Windows.Forms.Label();
            this.lbLevel = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.lbUsername = new System.Windows.Forms.Label();
            this.lbScore = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.nudLevel)).BeginInit();
            this.SuspendLayout();
            // 
            // listHighscores
            // 
            this.listHighscores.Font = new System.Drawing.Font("Consolas", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listHighscores.FormattingEnabled = true;
            this.listHighscores.ItemHeight = 32;
            this.listHighscores.Location = new System.Drawing.Point(12, 195);
            this.listHighscores.Name = "listHighscores";
            this.listHighscores.Size = new System.Drawing.Size(633, 420);
            this.listHighscores.TabIndex = 0;
            // 
            // lbHighscoresTitle
            // 
            this.lbHighscoresTitle.AutoSize = true;
            this.lbHighscoresTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbHighscoresTitle.Location = new System.Drawing.Point(12, 9);
            this.lbHighscoresTitle.Name = "lbHighscoresTitle";
            this.lbHighscoresTitle.Size = new System.Drawing.Size(463, 73);
            this.lbHighscoresTitle.TabIndex = 1;
            this.lbHighscoresTitle.Text = "HIGHSCORES";
            // 
            // comboGame
            // 
            this.comboGame.FormattingEnabled = true;
            this.comboGame.Items.AddRange(new object[] {
            "GyroBall",
            "AirHockey"});
            this.comboGame.Location = new System.Drawing.Point(502, 42);
            this.comboGame.Name = "comboGame";
            this.comboGame.Size = new System.Drawing.Size(121, 21);
            this.comboGame.TabIndex = 2;
            this.comboGame.SelectedIndexChanged += new System.EventHandler(this.comboGame_SelectedIndexChanged);
            // 
            // nudLevel
            // 
            this.nudLevel.Location = new System.Drawing.Point(503, 91);
            this.nudLevel.Maximum = new decimal(new int[] {
            20,
            0,
            0,
            0});
            this.nudLevel.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudLevel.Name = "nudLevel";
            this.nudLevel.Size = new System.Drawing.Size(120, 20);
            this.nudLevel.TabIndex = 3;
            this.nudLevel.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.nudLevel.ValueChanged += new System.EventHandler(this.nudLevel_ValueChanged);
            // 
            // lbGame
            // 
            this.lbGame.AutoSize = true;
            this.lbGame.Location = new System.Drawing.Point(503, 26);
            this.lbGame.Name = "lbGame";
            this.lbGame.Size = new System.Drawing.Size(35, 13);
            this.lbGame.TabIndex = 4;
            this.lbGame.Text = "Game";
            // 
            // lbLevel
            // 
            this.lbLevel.AutoSize = true;
            this.lbLevel.Location = new System.Drawing.Point(503, 75);
            this.lbLevel.Name = "lbLevel";
            this.lbLevel.Size = new System.Drawing.Size(33, 13);
            this.lbLevel.TabIndex = 5;
            this.lbLevel.Text = "Level";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 141);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(84, 31);
            this.label1.TabIndex = 6;
            this.label1.Text = "Level";
            // 
            // lbUsername
            // 
            this.lbUsername.AutoSize = true;
            this.lbUsername.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbUsername.Location = new System.Drawing.Point(313, 141);
            this.lbUsername.Name = "lbUsername";
            this.lbUsername.Size = new System.Drawing.Size(147, 31);
            this.lbUsername.TabIndex = 7;
            this.lbUsername.Text = "Username";
            // 
            // lbScore
            // 
            this.lbScore.AutoSize = true;
            this.lbScore.Font = new System.Drawing.Font("Microsoft Sans Serif", 20.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbScore.Location = new System.Drawing.Point(164, 141);
            this.lbScore.Name = "lbScore";
            this.lbScore.Size = new System.Drawing.Size(90, 31);
            this.lbScore.TabIndex = 8;
            this.lbScore.Text = "Score";
            // 
            // Highscores
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(657, 654);
            this.Controls.Add(this.lbScore);
            this.Controls.Add(this.lbUsername);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lbLevel);
            this.Controls.Add(this.lbGame);
            this.Controls.Add(this.nudLevel);
            this.Controls.Add(this.comboGame);
            this.Controls.Add(this.lbHighscoresTitle);
            this.Controls.Add(this.listHighscores);
            this.Name = "Highscores";
            this.Text = "Highscores";
            ((System.ComponentModel.ISupportInitialize)(this.nudLevel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ListBox listHighscores;
        private System.Windows.Forms.Label lbHighscoresTitle;
        private System.Windows.Forms.ComboBox comboGame;
        private System.Windows.Forms.NumericUpDown nudLevel;
        private System.Windows.Forms.Label lbGame;
        private System.Windows.Forms.Label lbLevel;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lbUsername;
        private System.Windows.Forms.Label lbScore;
    }
}