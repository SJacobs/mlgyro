﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MLGyro
{
    public partial class Highscores : Form
    {
        private Main main;

        public Highscores(Main main)
        {
            InitializeComponent();
            this.main = main;
            comboGame.SelectedIndex = 0;
            setupHighscores();
        }

        private void setupHighscores()
        {
            // List<string> highscores = main.dbManager.ExecuteReadQuery("SELECT Level, Score, Player_Name FROM Highscores h, Players p WHERE h.Player_ID = p.Player_ID");
            addItemsIntoList(grabHighscores());
        }

        private List<string> grabHighscores()
        {
            string gameName = Convert.ToString(comboGame.SelectedItem);
            int gameId = main.dbManager.getIdForGame(gameName);

            if (gameName != "AirHockey")
            {
                int level = Convert.ToInt32(nudLevel.Value);
                return main.dbManager.ExecuteReadQuery("SELECT Level, Score, Player_Name FROM Highscores h, Players p WHERE h.Player_ID = p.Player_ID AND Game_ID = " + gameId + " AND Level = " + level + " ORDER BY Score asc;");
            }
            return main.dbManager.ExecuteReadQuery("SELECT Score, Player_Name FROM Highscores h, Players p WHERE h.Player_ID = p.Player_ID AND Game_ID = " + gameId + " ORDER BY Score desc;");
        }

        private void addItemsIntoList(List<string> highscores)
        {
            listHighscores.Items.Clear();

            if (comboGame.SelectedItem.Equals("GyroBall"))
            {
                foreach (string highscore in highscores)
                {
                    char[] separatingChars = { '$' };
                    string[] texts = highscore.Split(separatingChars, System.StringSplitOptions.RemoveEmptyEntries);

                    listHighscores.Items.Add(texts[0] + space(texts[0]) + texts[1] + space(texts[1]) + texts[2]);
                }
            }
            else
            {
                listHighscores.Items.Add("Not added yet.");
            }
        }

        private string space(string text)
        {
            string space = "";
            for (int i = 0; i < (10 - text.Length); i++)
            {
                space += " ";
            }
            return space;
        }

        private void comboGame_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comboGame.SelectedItem.Equals("AirHockey"))
            {
                nudLevel.Enabled = false;
            }
            else
            {
                nudLevel.Enabled = true;
            }
            setupHighscores();
        }

        private void nudLevel_ValueChanged(object sender, EventArgs e)
        {
            setupHighscores();
        }
    }
}
