﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MLGyro
{
    public partial class Login : Form
    {
        private Main main;

        public Login(Main main)
        {
            InitializeComponent();
            this.main = main;
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            string name = tbUsername.Text;
            string password = tbPassword.Text;

           // main.dbManager.InsertPlayer(name, password);
            if (!main.dbManager.tryLogin(name, password))
            {
                invalidPass();
                return;
            }

            main.login(name);

            Close();
        }

        public void invalidPass()
        {
            lbPassWrong.Show();
            timerInvalidLogin.Start();
        }

        private void timerInvalidLogin_Tick(object sender, EventArgs e)
        {
            lbPassWrong.Hide();
            timerInvalidLogin.Stop();
        }
    }
}
