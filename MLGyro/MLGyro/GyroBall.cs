﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MLGyro.MLGyroGame.Levels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MLGyro.MLGyro
{
    public class GyroBall : AbstractGame
    {
        private const int ARROW_DIRECTION_SPEED = 7;

        private Main main;
        private Ball ball;
        private List<Tile> tiles = new List<Tile>();
        private int currentLevel = 0;
        public const string GAME_NAME = "GyroBall";

        private int currentTime = 0;

        public GyroBall(Main main, Ball ball)
        {
            this.main = main;
            this.ball = ball;
        }

        public void incrementTime()
        {
            if (BallOnStartTile())
            {
                currentTime = 0;
                return;
            }
            currentTime++;
        }

        public override void Draw(SpriteBatch spriteBatch, SpriteFont font, ContentManager content)
        {
            double thumbStickX = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X;
            double thumbStickY = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y;

            //spriteBatch.DrawString(font, "THUMB X: " + thumbStickX, new Vector2(20, 900), Color.Black);
            //spriteBatch.DrawString(font, "THUMB Y: " + thumbStickY, new Vector2(20, 1000), Color.Black);

            Color TEXT_COLOR = Color.Black;
            const int TEXT_X = 20;
            //spriteBatch.DrawString(font, "Ball on tile: " + (BallOnAnyTile() ? "TRUE" : "FALSE"), new Vector2(TEXT_X, 600), TEXT_COLOR);
            //spriteBatch.DrawString(font, "Direction: " + ball.direction, new Vector2(TEXT_X, 700), TEXT_COLOR);

            // Draw tiles.
            foreach (Tile tile in tiles)
            {
                spriteBatch.Draw(tile.image, tile.position, Color.White);
                if (tile.kind == Kind.START)
                {
                    spriteBatch.Draw(content.Load<Texture2D>("start"), tile.position, Color.White);
                    continue;
                }
                if (tile.kind == Kind.END)
                {
                    spriteBatch.Draw(content.Load<Texture2D>("stop"), tile.position, Color.White);
                    continue;
                }
                if (tile.direction != Direction.NONE)
                {
                    spriteBatch.Draw(content.Load<Texture2D>(getArrowDirection(tile.direction)), tile.position, Color.White);
                    continue;
                }
            }

            spriteBatch.DrawString(font, "LEVEL: " + currentLevel, new Vector2(20, 920), Color.Black);
            spriteBatch.DrawString(font, "TIME: " + currentTime, new Vector2(20, 1000), Color.Black);

            // Draw ball.
            spriteBatch.Draw(ball.image, ball.position, Color.White);
        }

        private string getArrowDirection(Direction direction)
        {
            string arrowName = null;
            switch (direction)
            {
                case Direction.LEFT:
                    arrowName = "arrowleft";
                    break;
                case Direction.RIGHT:
                    arrowName = "arrowright";
                    break;
                case Direction.UP:
                    arrowName = "arrowup";
                    break;
                case Direction.DOWN:
                    arrowName = "arrowdown";
                    break;
            }
            return arrowName;
        }

        public bool lightOn = false;
        public int lightTick = 0;
        public override void Update(GameTime gameTime)
        {
            /*if (lightOn)
            {
                lightTick += 1;
                if (lightTick == 10)
                {
                    main.communication.SendMessage("#2.setlight 0");
                    lightTick = 0;
                    lightOn = false;
                }
              
            }*/

            if (BallOnEndTile())
            {
                main.dbManager.InsertHighscore(main.username, GAME_NAME, currentLevel, currentTime);
                NextLevel(true);
            }

            if (!BallOnAnyTile())
            {
                NextLevel(false);
            }
            incrementTime();
            SetBallDirection();
            MoveBall();
        }


        public bool BallFallsOffPlatform(Tile tile, Vector2 position)
        {
            Rectangle ballRect = new Rectangle((int)position.X + (ball.image.Width / 2), (int)position.Y + (ball.image.Height / 2), 0, 0);
            Rectangle tileRect = new Rectangle((int)tile.position.X, (int)tile.position.Y, tile.image.Width + 1, tile.image.Height + 1);
            return ballRect.Intersects(tileRect);
        }

        /* public bool BallFallsOffAllPlatforms()
         {
             bool failed = false;
             foreach (Tile tile in tiles)
             {
                 if (BallOnAnyTile())
                 {
                     switch (tile.direction)
                     {
                         case Direction.UP:
                             ball.direction = Direction.UP;
                             break;
                         case Direction.DOWN:
                             ball.direction = Direction.DOWN;
                             break;
                         case Direction.LEFT:
                             ball.direction = Direction.LEFT;
                             break;
                         case Direction.RIGHT:
                             ball.direction = Direction.RIGHT;
                             break;
                         case Direction.NONE:
                             ball.direction = Direction.NONE;
                             break;
                     }
                 }
             }
             return failed;
         }*/

        public bool BallOnAnyTile()
        {
            bool ballOnAnyTile = false;
            foreach (Tile tile in tiles)
            {
                if (Main.BallOnSprite(ball, tile))
                {
                    ballOnAnyTile = true;
                    break;
                }
            }
            return ballOnAnyTile;
        }

        public bool BallOnStartTile()
        {
            foreach (Tile tile in tiles)
            {
                if (tile.kind == Kind.START)
                {
                    if (Main.BallOnSprite(ball, tile))
                    {
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }

        public bool BallOnEndTile()
        {
            foreach (Tile tile in tiles)
            {
                if (tile.kind == Kind.END)
                {
                    if (Main.BallOnSprite(ball, tile))
                    {
                        return true;
                    }
                    return false;
                }
            }
            return false;
        }

        public Direction GetDirectionOfCurrentTile()
        {
            Direction direction = ball.direction;
            foreach (Tile tile in tiles)
            {
                if (Main.BallOnSprite(ball, tile))
                {
                    if (!BallOnAnyTile())
                    {
                        direction = Direction.NONE;
                        break;
                    }
                    //if (tile.direction != Direction.NONE)
                    // {
                    direction = tile.direction;
                    break;
                    // }
                }
            }
            return direction;
        }

        public void NextLevel(bool incrementLevel)
        {
          /* main.communication.SendMessage("#2.setlight 1");
           lightOn = true;
           */

            ball.direction = Direction.NONE;
            if (incrementLevel)
            {
                currentLevel++;
            }
            switch (currentLevel)
            {
                case 1:
                    LoadLevel(new Level1());
                    break;
                case 2:
                    LoadLevel(new Level2());
                    break;
                case 3:
                    LoadLevel(new Level3());
                    break;
                case 4:
                    LoadLevel(new Level4());
                    break;
                case 5:
                    LoadLevel(new Level5());
                    break;
                case 6:
                    LoadLevel(new Level6());
                    break;
                case 7:
                    LoadLevel(new Level7());
                    break;
                default:
                    main.Exit();
                    break;
            }
            Console.WriteLine("Saved level.");
            if (!main.playingAsGuest)
            {
                main.dbManager.UpdateLevelForGame(main.username, main.dbManager.getIdForGame(GAME_NAME), currentLevel);
            }
        }

        public void LoadLevel(Level level)
        {
            tiles = level.tiles;
            ball.position = level.startPosition;
        }

        public bool PressedConfirmButton()
        {
            return Keyboard.GetState().IsKeyDown(Keys.Enter)
                || Keyboard.GetState().IsKeyDown(Keys.Space)
                || GamePad.GetState(PlayerIndex.One).Buttons.A == ButtonState.Pressed;
        }

        private void SetBallDirection()
        {
            if (BallOnAnyTile())
            {
                ball.direction = GetDirectionOfCurrentTile();
            }
            else
            {
                ball.direction = Direction.NONE;
            }
        }

        private void MoveBall()
        {
            switch (ball.direction)
            {
                case Direction.UP:
                    ball.position.Y -= ARROW_DIRECTION_SPEED;
                    break;
                case Direction.DOWN:
                    ball.position.Y += ARROW_DIRECTION_SPEED;
                    break;
                case Direction.LEFT:
                    ball.position.X -= ARROW_DIRECTION_SPEED;
                    break;
                case Direction.RIGHT:
                    ball.position.X += ARROW_DIRECTION_SPEED;
                    break;
                case Direction.NONE:
                    break;
            }
        }
    }
}
