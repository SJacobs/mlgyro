﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace MLGyro.MLGyroGame.Levels
{
    public abstract class Level
    {
        public List<Tile> tiles = new List<Tile>();
        public Vector2 startPosition;

        public Level()
        {
            setupLevel();
        }

        public abstract void setupLevel();

        public void addTile(Kind kind, Direction direction, int x, int y)
        {
           tiles.Add(new Tile(Main.tileImage, new Vector2(x, y), kind, direction));

            
            /*
            if (kind == Kind.START)
            {
                tiles.Add(new Tile(Main.tileStart, new Vector2(x, y), kind, Direction.NONE));
                return;
            }
            else if (kind == Kind.END)
            {
                tiles.Add(new Tile(Main.tileEnd, new Vector2(x, y), kind, Direction.NONE));
                return;
            }
            else
            {
                switch (direction)
                {
                    case Direction.NONE:
                        tiles.Add(new Tile(Main.tileImage, new Vector2(x, y), Kind.NORMAL, Direction.NONE));
                        break;
                    case Direction.UP:
                        tiles.Add(new Tile(Main.tileUpImage, new Vector2(x, y), Kind.NORMAL, Direction.UP));
                        break;
                    case Direction.DOWN:
                        tiles.Add(new Tile(Main.tileDownImage, new Vector2(x, y), Kind.NORMAL, Direction.DOWN));
                        break;
                    case Direction.LEFT:
                        tiles.Add(new Tile(Main.tileLeftImage, new Vector2(x, y), Kind.NORMAL, Direction.LEFT));
                        break;
                    case Direction.RIGHT:
                        tiles.Add(new Tile(Main.tileRightImage, new Vector2(x, y), Kind.NORMAL, Direction.RIGHT));
                        break;
                }
            }
            */
        }
    }
}