﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace MLGyro.MLGyroGame.Levels
{
    public class Level2 : Level
    {
        public Level2()
            : base()
        {
        }

        public override void setupLevel()
        {
            startPosition = new Vector2(0, 100);

            const int MOVE = 200;
            int x = 50;
            int y = 50;
            addTile(Kind.START, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y -= 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y -= 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y -= 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y -= 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.END, Direction.NONE, x, y);

        }
    }
}