﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace MLGyro.MLGyroGame.Levels
{
    public class Level6 : Level
    {
        public Level6()
            : base()
        {
        }

        public override void setupLevel()
        {
            int x = 50;
            int y = 50;
            addTile(Kind.START, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.UP, x, y);
            addTile(Kind.NORMAL, Direction.NONE, x+200, y);
            y -= 200;
            addTile(Kind.NORMAL, Direction.UP, x, y);
            y -= 200;
            addTile(Kind.NORMAL, Direction.RIGHT, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.RIGHT, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.DOWN, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.DOWN, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.RIGHT, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.END, Direction.NONE, x, y);

        }
    }
}
