﻿using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace MLGyro.MLGyroGame.Levels
{
    public class Level7 : Level
    {
        public Level7()
            : base()
        {
        }

        public override void setupLevel()
        {
            startPosition = new Vector2(0, 100);

            int x = 50;
            int y = 50;
            addTile(Kind.START, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.UP, x, y);

            addTile(Kind.NORMAL, Direction.NONE, x+200, y);
            addTile(Kind.NORMAL, Direction.UP, x, y+200);
            addTile(Kind.NORMAL, Direction.UP, x, y+400);
            addTile(Kind.NORMAL, Direction.LEFT, x+200, y + 400);

            addTile(Kind.NORMAL, Direction.LEFT,1050, 50);
            addTile(Kind.NORMAL, Direction.LEFT, 1250, 50);
            addTile(Kind.NORMAL, Direction.UP, 1250, 250);
            addTile(Kind.NORMAL, Direction.NONE, 1050, 450);

            y -= 200;
            addTile(Kind.NORMAL, Direction.UP, x, y);
            y -= 200;
            addTile(Kind.NORMAL, Direction.RIGHT, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.RIGHT, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.DOWN, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.DOWN, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.DOWN, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.DOWN, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.LEFT, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.LEFT, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.LEFT, x, y);
            y -= 200;
            addTile(Kind.NORMAL, Direction.DOWN, x, y);
            y -= 200;
            addTile(Kind.NORMAL, Direction.UP, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            x += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.NORMAL, Direction.NONE, x, y);
            y += 200;
            addTile(Kind.END, Direction.NONE, x, y);
        }
    }
}