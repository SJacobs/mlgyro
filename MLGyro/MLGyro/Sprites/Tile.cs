﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MLGyro
{
    public class Tile : Sprite
    {
        public Kind kind;
        public Direction direction;
        public Texture2D arrow;

        public Tile(Texture2D image, Vector2 position, Kind kind, Direction direction)
            : base(image, position)
        {
            this.image = image;
            this.position = position;
            this.kind = kind;
            this.direction = direction;
        }
    }
}