﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MLGyro.MLGyro;
using MLGyro.MLGyroGame.Levels;
using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace MLGyro
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Main : Game
    {

        /// ////////////airhockey
        GraphicsDeviceManager graphics;
        // SpriteBatch spriteBatch;

        //ball
        public Texture2D ballSprite;
        public Vector2 ballPosition = new Vector2(975, 500);
        public Vector2 ballspeed = new Vector2(150, 150);
        int ballRadius;
        float ballCentre_X;
        float ballCentre_Y;
        int ballmass;

        //left paddle
        public Texture2D paddleLeftSprite;
        public Vector2 paddleLeftPosition = new Vector2(250, 500);
        public Vector2 paddleLeftSpeed;
        int paddleLeftRadius;
        float paddleLeftCentre_X;
        float paddleLeftCentre_Y;
        int paddleLeftMass;

        //right paddle
        public Texture2D paddleRightSprite;
        public Vector2 paddleRightPosition = new Vector2(1550, 500);
        public Vector2 paddleRightSpeed;
        int paddleRightRadius;
        float paddleRightCentre_X;
        float paddleRightCentre_Y;
        int paddleRightMass;

        //overige informatie
        //SpriteFont font;
        int score = 0;
        int score2 = 0;

        public Texture2D goalRightSprite;
        public Vector2 goalRightPosition = new Vector2(1909, 400);
        public Texture2D goalLeftSprite;
        public Vector2 goalLeftPosition = new Vector2(0, 400);
        public Texture2D lineSprite;
        public Vector2 linePosition = new Vector2(400, -100);

        float newVelBall_X;
        float newVelBall_Y;

        double distance;
        double distance2;
        /// ///////////////////////






        private const bool COMMUNICATION_ENABLED = true;
        private const int SPEED = 12;

        private const bool FULL_SCREEN = false;

        // GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private Ball ball;

        public static Texture2D tileImage;
        public static Texture2D tileUpImage;
        public static Texture2D tileDownImage;
        public static Texture2D tileLeftImage;
        public static Texture2D tileRightImage;
        public static Texture2D tileStart;
        public static Texture2D tileEnd;
        private SpriteFont font;

        private Button gyroBallButton;
        private Button airHockeyButton;
        private Button hiscoresButton;
        private Button quitButton;
        private Button loginButton;
        private Button playGuestButton;
        private Button setPlayer;
        private Button activateGyro;
        private Button lightOn;
        private Button lightOff;

        private List<Tile> tiles = new List<Tile>();

        private bool inMenu = true;

        private Games currentGame;
        private AbstractGame game;

        public DatabaseManager dbManager = new DatabaseManager();
        public Communication communication;

        States currentState = States.LOGIN;

        public bool playingAsGuest = true;
        public List<Player> players = new List<Player>();

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1920;
            graphics.PreferredBackBufferHeight = 1080;
            graphics.IsFullScreen = FULL_SCREEN;
            Content.RootDirectory = "Content";

            players.Add(new Player(0, "Guest"));
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();

            //addTiles();

            if (COMMUNICATION_ENABLED)
            {
                communication = new Communication();
            }

            if (COMMUNICATION_ENABLED)
            {
                foreach (Player player in players)
                {
                    communication.assignControllerToPlayer(player, new Controller(1));
                }
            }

            //  System.Threading.Thread.Sleep(5000);
            // communication.SendMessage("#2.setplayer 1");

            /* if (COMMUNICATION_ENABLED)
             {
                 communication.getControllerId();
             }*/
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            ////////////////////airhockey
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            spriteBatch = new SpriteBatch(GraphicsDevice);

            ballSprite = Content.Load<Texture2D>("puck");
            paddleLeftSprite = Content.Load<Texture2D>("paddle2");
            paddleRightSprite = Content.Load<Texture2D>("paddle");

            goalLeftSprite = Content.Load<Texture2D>("circle2");
            goalRightSprite = Content.Load<Texture2D>("circle1");
            lineSprite = Content.Load<Texture2D>("line");
            font = Content.Load<SpriteFont>("Score");
            ///////////////////////////////////


            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            Texture2D ballImage = Content.Load<Texture2D>("ball");
            Vector2 position = new Vector2(500, 500);
            ball = new Ball(ballImage, position, Direction.NONE);

            tileImage = Content.Load<Texture2D>("tile");

            Texture2D buttonImage = Content.Load<Texture2D>("button");

            gyroBallButton = new Button(buttonImage, new Vector2(300, 200), "Play Gyroball");
            airHockeyButton = new Button(buttonImage, new Vector2(300, 400), "Play AirHockey");
            hiscoresButton = new Button(buttonImage, new Vector2(300, 600), "Hiscores");
            quitButton = new Button(buttonImage, new Vector2(300, 800), "Quit");

            loginButton = new Button(Content.Load<Texture2D>("button"), new Vector2(300, 200), "Login");
            playGuestButton = new Button(Content.Load<Texture2D>("button"), new Vector2(300, 400), "Guest");
            setPlayer = new Button(Content.Load<Texture2D>("button"), new Vector2(300, 600), "Set player");
            activateGyro = new Button(Content.Load<Texture2D>("button"), new Vector2(650, 600), "Activate gyro");
            lightOn = new Button(Content.Load<Texture2D>("button"), new Vector2(1000, 600), "Light On");
            lightOff = new Button(Content.Load<Texture2D>("button"), new Vector2(1350, 600), "Light Off");

            font = Content.Load<SpriteFont>("font");
            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // Close serial port.
            //  communication.serialPort.Close();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
            {
                currentState = States.MENU;
            }
            if (COMMUNICATION_ENABLED)
            {
                communication.tick();
            }
            if (currentGame == Games.AIRHOCKEY)
            {
                Airhockey.x = -communication.gyroY;
                Airhockey.y = -communication.gyroX;
                double thumbStickX = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X;
                double thumbStickY = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y;
                Airhockey.xboxX = thumbStickX;
                Airhockey.xboxY = thumbStickY;
                game.Update(gameTime);
                return;
            }
            CheckControls();

            switch (currentState)
            {
                case States.LOGIN:
                    handleLogin();
                    break;
                case States.MENU:
                    HandleMenu();
                    break;
                case States.GAME:
                    if (game != null)
                    {
                        game.Update(gameTime);
                    }
                    break;
            }

            base.Update(gameTime);
        }


        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            double thumbStickX = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X;
            double thumbStickY = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y;

            ClearScreen();

            spriteBatch.Begin();

            switch (currentState)
            {
                case States.LOGIN:
                    DrawLogin();
                    break;
                case States.MENU:
                    DrawMenu();
                    break;
                case States.GAME:
                    game.Draw(spriteBatch, font, Content);
                    break;
            }

            if (currentGame == Games.GYROBALL)
            {
                DrawBall();
            }
            spriteBatch.End();

            base.Draw(gameTime);
        }

        public static bool BallOnSprite(Ball ball, Sprite sprite)
        {
            int middleOfBallX = (int)ball.position.X + ((ball.image.Width) / 2);
            int middleOfBallY = (int)ball.position.Y + ((ball.image.Height) / 2);

            return (middleOfBallX - 1) < (sprite.position.X + sprite.image.Width)
                && (middleOfBallX + 1) > sprite.position.X
                && (middleOfBallY + 1) > sprite.position.Y
                && (middleOfBallY - 1) < (sprite.position.Y + sprite.image.Height);
        }

        GamePadState prevGamePadState;
        GamePadState gamePadState;
        public bool PressedConfirmButton()
        {
            if (communication.buttonPressed)
            {
                communication.buttonPressed = false;
                return true;
            }
            prevKeyState = keyState;
            keyState = Keyboard.GetState();
            prevGamePadState = gamePadState;
            gamePadState = GamePad.GetState(PlayerIndex.One);
            return (keyState.IsKeyDown(Keys.Enter) && prevKeyState.IsKeyUp(Keys.Enter))
                || (keyState.IsKeyDown(Keys.Space) && prevKeyState.IsKeyUp(Keys.Space))
                || (gamePadState.Buttons.A == ButtonState.Pressed && prevGamePadState.Buttons.A == ButtonState.Released);
        }

        public void ClearScreen()
        {
            if (currentState != States.GAME)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);
                return;
            }

            if (currentGame == Games.GYROBALL)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);
                return;
            }

            if (currentGame == Games.AIRHOCKEY)
            {
                GraphicsDevice.Clear(Color.CornflowerBlue);
                return;
            }
        }

        public void DrawButtonWithText(Button button)
        {
            spriteBatch.Draw(button.image, button.position, Color.White);
            if (button.text.Split(' ').Length > 1)
            {
                string[] texts = button.text.Split(' ');
                string firstPart = texts[0];
                string secondPart = texts[1];
                spriteBatch.DrawString(font, firstPart, new Vector2(button.position.X + 20, button.position.Y + 10), Color.Black);
                spriteBatch.DrawString(font, secondPart, new Vector2(button.position.X + 20, button.position.Y + 60 + 10), Color.Black);
                return;
            }
            spriteBatch.DrawString(font, button.text, new Vector2(button.position.X + 20, button.position.Y + 10), Color.Black);
        }

        public void DrawLogin()
        {
            DrawButtonWithText(loginButton);
            DrawButtonWithText(playGuestButton);
            DrawButtonWithText(setPlayer);
            DrawButtonWithText(activateGyro);
            DrawButtonWithText(lightOn);
            DrawButtonWithText(lightOff);
            DrawButtonWithText(quitButton);
        }

        public void DrawBall()
        {
            spriteBatch.Draw(ball.image, ball.position, Color.White);
        }

        public string username;
        public void DrawMenu()
        {
            double thumbStickX = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X;
            double thumbStickY = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y;

            DrawButtonWithText(gyroBallButton);
            DrawButtonWithText(airHockeyButton);
            DrawButtonWithText(hiscoresButton);
            DrawButtonWithText(quitButton);

            spriteBatch.DrawString(font, "MENU", new Vector2(20, 50), Color.Black);
            // spriteBatch.DrawString(font, "THUMB X: " + thumbStickX, new Vector2(20, 900), Color.Black);
            // spriteBatch.DrawString(font, "THUMB Y: " + thumbStickY, new Vector2(20, 1000), Color.Black);

            spriteBatch.DrawString(font, "User:", new Vector2(20, 920), Color.Black);
            spriteBatch.DrawString(font, (username == null ? "Guest" : username), new Vector2(20, 1000), Color.Black);
        }

        public void login(string username)
        {
            this.username = username;
            currentState = States.MENU;
        }

        private Login loginForm;

        public void handleCommButton()
        {
            if (loginForm == null)
            {
                playingAsGuest = false;
                loginForm = new Login(this);
                loginForm.Show();
            }
        }

        private void handleLogin()
        {
            if (PressedConfirmButton())
            {
                if (BallOnSprite(ball, loginButton))
                {
                    if (loginForm == null)
                    {
                        playingAsGuest = false;
                        loginForm = new Login(this);
                        loginForm.Show();
                    }
                    return;
                }

                if (BallOnSprite(ball, setPlayer))
                {
                    //new SerialSelector(this).Show();
                    communication.SendMessage("#2.setplayer 1");
                    return;
                }

                if (BallOnSprite(ball, activateGyro))
                {
                    communication.SendMessage("#2.sensactive 1");
                    return;
                }

                if (BallOnSprite(ball, lightOn))
                {
                    communication.SendMessage("#2.setlight 1");
                    return;
                }
                if (BallOnSprite(ball, lightOff))
                {
                    communication.SendMessage("#2.setlight 0");
                    return;
                }
                if (BallOnSprite(ball, playGuestButton))
                {
                    currentState = States.MENU;
                    return;
                }

                if (BallOnSprite(ball, quitButton))
                {
                    Exit();

                    return;
                }
            }
        }

        KeyboardState prevKeyState;
        KeyboardState keyState;
        DateTime databaseButtonDelay = DateTime.Now;
        bool inLogin = true;
        private bool keySinglePressed(Keys key)
        {
            return keyState.IsKeyDown(key) && prevKeyState.IsKeyUp(key);
        }

        private void HandleMenu()
        {
            if (PressedConfirmButton())
            {
                if (BallOnSprite(ball, gyroBallButton))
                {
                    currentState = States.GAME;
                    currentGame = Games.GYROBALL;
                    game = new GyroBall(this, ball);
                    ((GyroBall)game).NextLevel(true);

                    return;
                }

                if (BallOnSprite(ball, airHockeyButton))
                {
                    currentState = States.GAME;
                    currentGame = Games.AIRHOCKEY;
                    game = new Airhockey(this, ball);
                    //((Airhockey)game).NextLevel(true);
                    return;
                }

                if (BallOnSprite(ball, hiscoresButton) && ((DateTime.Now - databaseButtonDelay).TotalMilliseconds) >= 1000)
                {
                    databaseButtonDelay = DateTime.Now;
                    new Highscores(this).Show();
                    return;
                }
                if (BallOnSprite(ball, quitButton))
                {
                    inMenu = false;
                    Exit();
                    return;
                }
            }
        }

        private void CheckControls()
        {
            double thumbStickX = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.X;
            double thumbStickY = GamePad.GetState(PlayerIndex.One).ThumbSticks.Left.Y;

            /* if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
             {
                 Exit();
             }
             */
            // Keyboard controlling
            if (Keyboard.GetState().IsKeyDown(Keys.Right))
            {
                ball.position.X += SPEED;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Left))
            {
                ball.position.X -= SPEED;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Up))
            {
                ball.position.Y -= SPEED;
            }
            if (Keyboard.GetState().IsKeyDown(Keys.Down))
            {
                ball.position.Y += SPEED;
            }

            // Xbox controller
            ball.position.X += (float)(thumbStickX * SPEED);
            ball.position.Y -= (float)(thumbStickY * SPEED);

            // Gyro!
            if (COMMUNICATION_ENABLED)
            {
                int x = communication.gyroX;
                int y = communication.gyroY;
                // Console.WriteLine("x: " + x + " y: "+ y);
                ball.position.X -= (y / 2);
                ball.position.Y -= (x / 2);

                /*
                if (x > 5)
                {
                    ball.position.X += (5 / 100 * x);
                }
                else if (x < -5)
                {
                    ball.position.X -= SPEED;
                }
                else if (communication.gyroY > 5)
                {
                    ball.position.Y -= SPEED;
                }
                else if (communication.gyroY < -5)
                {
                    ball.position.Y += SPEED;
                }
                */
            }

            if (ball.position.X > Window.ClientBounds.Width - (ball.image.Width / 2))
            {
                ball.position.X = Window.ClientBounds.Width - (ball.image.Width / 2);
            }
            if (ball.position.X < -ball.image.Width / 2)
            {
                ball.position.X = -ball.image.Width / 2;
            }

            if (ball.position.Y > Window.ClientBounds.Height - (ball.image.Height / 2))
            {
                ball.position.Y = Window.ClientBounds.Height - (ball.image.Height / 2);
            }
            if (ball.position.Y < 0 - (ball.image.Width / 2))
            {
                ball.position.Y = 0 - (ball.image.Width / 2);
            }
        }
    }
}
