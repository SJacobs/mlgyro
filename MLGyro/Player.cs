﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MLGyro
{
    public class Player
    {
        public int id;
        public string username;
        public int score;

        /// <summary>
        /// In the future, grab information from SQL DB.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="controller"></param>
        public Player(int id, string username)
        {
            this.id = id;
            this.username = username;
            score = 0;
        }
    }
}
