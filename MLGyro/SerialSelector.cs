﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO.Ports;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace MLGyro
{
    public partial class SerialSelector : Form
    {
        private Main main;

        public SerialSelector(Main main)
        {
            InitializeComponent();
            this.main = main;
            addSerialPortsToBox();
            setPortToLastIndex();
        }

        private void setPortToLastIndex()
        {
            serialPortSelectionBox.SelectedIndex = serialPortSelectionBox.Items.Count - 1;
        }

        private void connectButton_Click(object sender, EventArgs e)
        {
            if (main.communication.serialPort.IsOpen)
            {
                Console.WriteLine("1");
                main.communication.serialPort.Close();
            }
            else
            {
                Console.WriteLine("2");
                String port = serialPortSelectionBox.Text;
                try
                {
                    main.communication.serialPort.PortName = port;
                    main.communication.serialPort.Open();
                    if (main.communication.serialPort.IsOpen)
                    {
                        main.communication.serialPort.DiscardInBuffer();
                        main.communication.serialPort.DiscardOutBuffer();
                    }
                }
                catch (Exception exception)
                {
                    MessageBox.Show("Could not connect to the given serial port: " + exception.Message);
                }
            }
        }


        private void refreshSerialPortsButton_Click(object sender, EventArgs e)
        {
            addSerialPortsToBox();
        }

        private void addSerialPortsToBox()
        {
            String[] ports = SerialPort.GetPortNames();
            Array.Sort(ports);

            serialPortSelectionBox.Items.Clear();
            foreach (String port in ports)
            {
                serialPortSelectionBox.Items.Add(port);
            }
        }

        private void serialPortSelectionBox_Leave(object sender, EventArgs e)
        {
            serialPortSelectionBox.Text = serialPortSelectionBox.Text.ToUpper();
        }
    }
}
